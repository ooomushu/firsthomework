package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    EditText editText1;
    EditText editText2;
    TextView textView;
    TextView textView1;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = findViewById(R.id.button1);
        btn2 = findViewById(R.id.button2);
        btn3 = findViewById(R.id.button3);
        btn4 = findViewById(R.id.button4);
        editText1 = findViewById(R.id.edit1);
        editText2 = findViewById(R.id.edit2);
        textView = findViewById(R.id.text);
        textView1 = findViewById(R.id.text1);

        btn2.setOnTouchListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
btn1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        startActivity(intent);
    }
});
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            String setText = "Данная кнопка просто красивая!\nОна ничего не умеет делать.";
            textView.setText(setText);

        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            textView.setText("");
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button3:
                String setText = ", Вы зарегистрировались усешно!";
                textView1.setText(getResources().getString(R.string.format_string,editText1.getText().toString(),editText2.getText().toString(),setText));
                break;
            case R.id.button4:
                textView1.setText("");
                break;
        }
    }

}
